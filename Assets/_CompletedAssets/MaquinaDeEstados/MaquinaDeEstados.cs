﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaquinaDeEstados : MonoBehaviour 
{
	public Estado estadoPatrulla;
	public Estado estadoAlerta;
	public Estado estadoPersecusion;
	public Estado estadoInicial;

	public MeshRenderer meshRendererIndicador;

	private Estado estadoActual;

	void Start () 
	{
		ActicarEstado(estadoInicial);
	}

	//maquinaDeEstados.meshRendererIndicador.material.color = colorEstado;

	public void ActicarEstado(Estado nuevoEstado)
	{
		if (estadoActual != null) estadoActual.enabled = false;
		estadoActual = nuevoEstado;
		estadoActual.enabled = true;

		meshRendererIndicador.material.color = estadoActual.colorEstado;
	}
}
