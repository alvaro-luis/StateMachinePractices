﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorVision : MonoBehaviour 
{
	public Transform ojos;
	public float rangoVision = 20f;
	public Vector3 offset = new Vector3(0f, 0.75f, 0f);

	private ControladorNavMesh controladorNavMesh;

	void Awake()
	{
		controladorNavMesh = GetComponent<ControladorNavMesh>();
	}

	public bool PuedeVerAlJugador(out RaycastHit hit, bool mirarhaciaElJugador = false)
	{
		Vector3 vectorDireccion;
		if (mirarhaciaElJugador)
		{
			vectorDireccion = (controladorNavMesh.perseguirObjetivo.position + offset) - ojos.position;
		}
		else
		{
			vectorDireccion = ojos.forward;
		}

		// bool objetoQueMira = Physics.Raycast(ojos.position, vectorDireccion, out hit, rangoVision);
		// bool esJugador = hit.collider.CompareTag("Player");
		
		return Physics.Raycast(ojos.position, vectorDireccion, out hit, rangoVision) && hit.collider.CompareTag("Player");
	}
}