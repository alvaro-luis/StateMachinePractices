﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoAlerta : Estado 
{ 
	public float velocidadGiroBusqueda = 120f;
	public float duracionBusqueda = 4f;

	
	private ControladorNavMesh controladorNavMesh;
	private ControladorVision controladorVision;
	private float tiempoBuscando;

	// Use this for initialization
	protected override void Awake()
	{
		base.Awake();
		controladorNavMesh = GetComponent<ControladorNavMesh>();
		controladorVision = GetComponent<ControladorVision>();
	}

	void OnEnable()
	{
		//maquinaDeEstados.meshRendererIndicador.material.color = colorEstado;
		controladorNavMesh.DetenerNavMeshAgent();
		tiempoBuscando = 0f;
	}

	void Update ()
	{
		//Ve al jugador?
		RaycastHit hit;
		if (controladorVision.PuedeVerAlJugador(out hit))
		{
			controladorNavMesh.perseguirObjetivo = hit.transform;
			maquinaDeEstados.ActicarEstado(maquinaDeEstados.estadoPersecusion);
			return;
		}

		transform.Rotate(0f, velocidadGiroBusqueda * Time.deltaTime, 0f);
		tiempoBuscando += Time.deltaTime;
		if (tiempoBuscando >= duracionBusqueda)
		{
			maquinaDeEstados.ActicarEstado(maquinaDeEstados.estadoPatrulla);
			return;
		}
	}
}