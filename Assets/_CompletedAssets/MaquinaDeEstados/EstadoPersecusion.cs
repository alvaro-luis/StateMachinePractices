﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoPersecusion : Estado 
{
	private ControladorNavMesh controladorNavMesh;
	private ControladorVision controladorVision;


	protected override void Awake()
	{
		base.Awake();
		controladorNavMesh = GetComponent<ControladorNavMesh>();
		controladorVision = GetComponent<ControladorVision>();
	}

	void OnEnable()
	{
		//maquinaDeEstados.meshRendererIndicador.material.color = colorEstado;
	}

	void Update ()
	{
		RaycastHit hit;
		if (!controladorVision.PuedeVerAlJugador(out hit, true))
		{
			maquinaDeEstados.ActicarEstado(maquinaDeEstados.estadoAlerta);
			return;
		}

		controladorNavMesh.ActualizarPuntoDestinoNavMeshAgent();

	}
}