﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoPatrulla : Estado 
{
	public Transform[] wayPoints;

	private ControladorNavMesh controladorNavMesh;
	private ControladorVision controladorVision;
	private int siguienteWayPoint;

	protected override void Awake()
	{
		base.Awake();
		controladorNavMesh = GetComponent<ControladorNavMesh>();
		controladorVision = GetComponent<ControladorVision>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		//Ve al jugador?
		RaycastHit hit;
		if (controladorVision.PuedeVerAlJugador(out hit))
		{
			controladorNavMesh.perseguirObjetivo = hit.transform;
			maquinaDeEstados.ActicarEstado(maquinaDeEstados.estadoPersecusion);
			return;
		}

		if (controladorNavMesh.HemosLlegado())
		{
			siguienteWayPoint = (siguienteWayPoint + 1) % wayPoints.Length;
			ActualizarWayPointDestino();
		}
	}

	void OnEnable()
	{
		//maquinaDeEstados.meshRendererIndicador.material .color = colorEstado;
		ActualizarWayPointDestino();
	}

	void ActualizarWayPointDestino()
	{
		//dirigire al waypoint que toca en ese momento
		controladorNavMesh.ActualizarPuntoDestinoNavMeshAgent(wayPoints[siguienteWayPoint].position);
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player") && enabled)
		{
			maquinaDeEstados.ActicarEstado(maquinaDeEstados.estadoAlerta);
		}
	}
}