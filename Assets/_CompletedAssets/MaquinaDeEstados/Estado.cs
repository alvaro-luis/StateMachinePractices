﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estado : MonoBehaviour
{
	public Color colorEstado;
	protected MaquinaDeEstados maquinaDeEstados;

	protected virtual void Awake()
	{
		maquinaDeEstados = GetComponent<MaquinaDeEstados>();
	}
}